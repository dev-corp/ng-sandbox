import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'loading-demo', loadChildren: () => import('app/modules/loading-demo').then(m => m.LoadingDemoModule) },
  { path: 'notifications-demo', loadChildren: () => import('app/modules/notifications-demo').then(m => m.NotificationsDemoModule) },
  { path: 'nested-pages-demo', loadChildren: () => import('app/modules/nested-pages-demo').then(m => m.NestedPagesDemoModule) },
  { path: 'bubble-chart-demo', loadChildren: () => import('app/modules/bubble-chart-demo').then(m => m.BubbleChartDemoModule) },
  {
    path: 'json-placeholder-api-demo',
    loadChildren: () => import('app/modules/json-placeholder-api-demo').then(m => m.JsonPlaceholderApiDemoModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
