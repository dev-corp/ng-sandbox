import { Action } from '@ngrx/store';

export enum AppActionTypes {
  START_LOADING = '[App] START_LOADING',
  END_LOADING = '[App] END_LOADING',
}

export class StartLoading implements Action {
  readonly type = AppActionTypes.START_LOADING; // tslint:disable-line:typedef
}

export class EndLoading implements Action {
  readonly type = AppActionTypes.END_LOADING; // tslint:disable-line:typedef
}

export type AppAction = StartLoading | EndLoading;
