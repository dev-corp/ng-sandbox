import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';

import { AppState } from './app.interface';

export const selectAppFeature = createFeatureSelector<AppState>('app');

export const selectAppLoading: MemoizedSelector<any, boolean> = createSelector(
  selectAppFeature,
  (state) => !!state.loading
);

export const selectAppLoadingCount: MemoizedSelector<any, number> = createSelector(
  selectAppFeature,
  (state) => state.loading
);
