import { AppAction, AppActionTypes } from './app.actions';
import { AppState } from './app.interface';

export function appReducer(state: AppState, action: AppAction): AppState {
  switch (action.type) {
    case AppActionTypes.START_LOADING: {
      return { ...state, ...{ loading: state.loading + 1 } };
    }
    case AppActionTypes.END_LOADING: {
      return { ...state, ...{ loading: state.loading - 1 } };
    }
    default: {
      return state;
    }
  }
}
