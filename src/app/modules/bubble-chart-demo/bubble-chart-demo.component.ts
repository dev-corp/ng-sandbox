import { Component, OnInit } from '@angular/core';
import { nlpData } from 'src/app/shared/components/ned-graph/ned-mock-data';
import { NEDGraphData } from 'app/shared/components/ned-graph/ned-graph-data.interface';

@Component({
  selector: 'ns-bubble-chart-demo',
  templateUrl: './bubble-chart-demo.component.html',
  styleUrls: ['./bubble-chart-demo.component.scss']
})
export class BubbleChartDemoComponent implements OnInit {
  nlpData: NEDGraphData[] = nlpData;

  constructor() { }

  ngOnInit(): void {
  }

}
