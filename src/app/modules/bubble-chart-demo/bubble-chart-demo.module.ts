import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BubbleChartDemoComponent } from './bubble-chart-demo.component';
import { BubbleChartDemoRoutingModule } from './bubble-chart-demo-routing.module';
import { NedGraphModule } from 'app/shared/components/ned-graph/ned-graph.module';

@NgModule({
  declarations: [BubbleChartDemoComponent],
  imports: [
    CommonModule,
    BubbleChartDemoRoutingModule,
    NedGraphModule
  ]
})
export class BubbleChartDemoModule { }
