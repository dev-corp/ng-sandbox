import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BubbleChartDemoComponent } from './bubble-chart-demo.component';
const routes: Routes = [
  {
    path: '', component: BubbleChartDemoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BubbleChartDemoRoutingModule {}
