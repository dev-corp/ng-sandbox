import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingIndicatorModule } from 'ns-lib';

import { LoadingDemoRoutingModule } from './loading-demo-routing.module';
import { LoadingDemoComponent } from './loading-demo.component';

@NgModule({
  declarations: [LoadingDemoComponent],
  imports: [
    CommonModule,
    LoadingDemoRoutingModule,
    LoadingIndicatorModule
  ],
})
export class LoadingDemoModule { }
