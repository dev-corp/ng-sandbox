import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppActionTypes, selectAppLoading, selectAppLoadingCount } from 'app/core/+state';
import { NgUnsubscribe, StoreUtil } from 'app/shared/utilities';

@Component({
  selector: 'ns-loading-demo',
  templateUrl: './loading-demo.component.html',
  styleUrls: ['./loading-demo.component.scss']
})
export class LoadingDemoComponent extends NgUnsubscribe {

  private loading$: Observable<boolean>;
  private loadingCount$: Observable<number>;

  constructor(private store: Store<any>) {
    super();
    // this.loading$ = this.store.select(selectAppLoading);
    // this.loadingCount$ = this.store.select(selectAppLoadingCount);
    this.loading$ = StoreUtil.select(this.store, selectAppLoading, this.ngUnsubscribe);
    this.loadingCount$ = StoreUtil.select(this.store, selectAppLoadingCount, this.ngUnsubscribe);
  }

  public startLoading(): void {
    this.store.dispatch({ type: AppActionTypes.START_LOADING });
  }

  public stopLoading(): void {
    this.store.dispatch({ type: AppActionTypes.END_LOADING });
  }
}
