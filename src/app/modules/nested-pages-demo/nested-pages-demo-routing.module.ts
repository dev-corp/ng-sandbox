import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NestedPagesDemoComponent } from './nested-pages-demo.component';
import { ViewOneComponent } from './views/view-one/view-one.component';
import { ViewTwoComponent } from './views/view-two/view-two.component';
import { ViewThreeComponent } from './views/view-three/view-three.component';

let routes: Routes;
routes = [
  {
    path: '', component: NestedPagesDemoComponent, children: [
      { path: 'one', component: ViewOneComponent },
      { path: 'two', component: ViewTwoComponent },
      {
        path: 'three', component: ViewThreeComponent, children: [
          { path: 'two', component: ViewTwoComponent },
          { path: 'nested-loading-demo', loadChildren: () => import('app/modules/loading-demo').then(m => m.LoadingDemoModule) },
        ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NestedPagesDemoRoutingModule {}
