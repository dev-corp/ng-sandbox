import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NestedPagesDemoRoutingModule } from './nested-pages-demo-routing.module';
import { NestedPagesDemoComponent } from './nested-pages-demo.component';
import { ViewOneComponent } from './views/view-one/view-one.component';
import { ViewTwoComponent } from './views/view-two/view-two.component';
import { ViewThreeComponent } from './views/view-three/view-three.component';

@NgModule({
  declarations: [
    NestedPagesDemoComponent,
    ViewOneComponent,
    ViewTwoComponent,
    ViewThreeComponent
  ],
  imports: [
    CommonModule,
    NestedPagesDemoRoutingModule
  ]
})
export class NestedPagesDemoModule {}
