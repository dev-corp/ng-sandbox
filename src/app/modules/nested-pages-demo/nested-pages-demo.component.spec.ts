import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NestedPagesDemoComponent } from './nested-pages-demo.component';

describe('NestedPagesDemoComponent', () => {
  let component: NestedPagesDemoComponent;
  let fixture: ComponentFixture<NestedPagesDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NestedPagesDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NestedPagesDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
