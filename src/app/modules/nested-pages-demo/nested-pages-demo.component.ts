import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ns-nested-pages-demo',
  templateUrl: './nested-pages-demo.component.html',
  styleUrls: ['./nested-pages-demo.component.scss']
})
export class NestedPagesDemoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
