import { Component, OnInit } from '@angular/core';

// import { NotificationService } from 'app/shared/components/notification-stateful';
import { NotificationService } from 'app/shared/components/notification-redux';

@Component({
  selector: 'ns-notifications-demo',
  templateUrl: './notifications-demo.component.html',
  styleUrls: ['./notifications-demo.component.scss']
})
export class NotificationsDemoComponent implements OnInit {

  private count: number = 0;

  constructor(private notificationService: NotificationService) { }

  ngOnInit(): void {
  }

  public showNotification(): void {
    this.notificationService.addNotification('Hello world ' + this.count++, { closable: true, duration: 4000 });
  }
}
