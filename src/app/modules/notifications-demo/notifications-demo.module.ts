import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { NotificationModule } from 'app/shared/components/notification-stateful';
import { NotificationModule } from 'app/shared/components/notification-redux';

import { NotificationsDemoRoutingModule } from './notifications-demo-routing.module';
import { NotificationsDemoComponent } from './notifications-demo.component';

@NgModule({
  declarations: [NotificationsDemoComponent],
  imports: [
    CommonModule,
    NotificationsDemoRoutingModule,
    NotificationModule
  ]
})
export class NotificationsDemoModule {}
