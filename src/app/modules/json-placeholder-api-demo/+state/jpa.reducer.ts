import { JPAState } from './jpa.interface';
import { JPAAction, JPAActionTypes } from './jpa.actions';

export function jpaReducer(state: JPAState, action: JPAAction): JPAState {
  switch (action.type) {
    case JPAActionTypes.LOAD_TODO: {
      return { ...state, ...{ todo: action.payload } };
    }
    case JPAActionTypes.LOAD_TODOS: {
      return { ...state, ...{ todos: action.payload } };
    }
    default: {
      return state;
    }
  }
}
