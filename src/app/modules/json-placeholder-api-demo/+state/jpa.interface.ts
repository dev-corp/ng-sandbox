import { Todo } from 'app/shared/models';

export interface JPAState {
  todos: Todo[];
  todo: Todo;
}
