import { JPAState } from './jpa.interface';

export const jpaInitialState: JPAState = {
  todo: null,
  todos: []
};
