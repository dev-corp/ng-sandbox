import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { JPAState } from './jpa.interface';
import { Todo } from 'app/shared/models';

const selectTodosFeature = createFeatureSelector<JPAState>('jpa');

export class FromJPAState {

  public static selectCurrentTodo: MemoizedSelector<any, Todo> = createSelector(
    selectTodosFeature,
    (state) => state.todo
  );

  public static selectTodoList: MemoizedSelector<any, Todo[]> = createSelector(
    selectTodosFeature,
    (state) => state.todos
  );

}
