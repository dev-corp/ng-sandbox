import { Action } from '@ngrx/store';
import { Todo } from 'app/shared/models';

export enum JPAActionTypes {
  LOAD_TODOS = '[JPA.Todos] LOAD_TODOS',
  LOAD_TODO = '[JPA.Todos] LOAD_TODO'
}

export class LoadTodos implements Action {
  readonly type = JPAActionTypes.LOAD_TODOS; // tslint:disable-line:typedef

  constructor(public payload: Todo[]) {}
}

export class LoadTodo implements Action {
  readonly type = JPAActionTypes.LOAD_TODO; // tslint:disable-line:typedef

  constructor(public payload: Todo) {}
}

export type JPAAction = LoadTodos | LoadTodo;
