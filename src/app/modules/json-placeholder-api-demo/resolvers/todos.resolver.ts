import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { Todo } from 'app/shared/models';
import { TodoResource } from 'app/shared/resources';
import { JPAActionTypes } from '../+state';

@Injectable()
export class TodosResolver implements Resolve<Todo[]> {

  constructor(private store: Store<any>,
              private todoResource: TodoResource) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Todo[]> | Promise<Todo[]> | Todo[] {
    return this.todoResource.getTodos().pipe(
      tap(todos => {
        this.store.dispatch({ type: JPAActionTypes.LOAD_TODOS, payload: todos });
      })
    );
  }
}
