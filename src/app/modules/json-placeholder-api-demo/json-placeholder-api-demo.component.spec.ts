import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JsonPlaceholderApiDemoComponent } from './json-placeholder-api-demo.component';

describe('JsonPlaceholderApiDemoComponent', () => {
  let component: JsonPlaceholderApiDemoComponent;
  let fixture: ComponentFixture<JsonPlaceholderApiDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JsonPlaceholderApiDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JsonPlaceholderApiDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
