import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Todo } from 'app/shared/models';
import { StoreUtil } from 'app/shared/utilities';

import { FromJPAState } from '../../+state';

@Component({
  selector: 'ns-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent {

  public todos$: Observable<Todo[]>;

  constructor(private store: Store<any>) {
    this.todos$ = StoreUtil.select(this.store, FromJPAState.selectTodoList);
  }

}
