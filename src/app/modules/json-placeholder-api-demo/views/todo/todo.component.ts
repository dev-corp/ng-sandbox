import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Todo } from 'app/shared/models';
import { StoreUtil } from 'app/shared/utilities';

import { FromJPAState } from '../../+state';

@Component({
  selector: 'ns-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent {
  private todo$: Observable<Todo>;

  constructor(private store: Store<any>) {
    this.todo$ = StoreUtil.select(this.store, FromJPAState.selectCurrentTodo);
  }
}
