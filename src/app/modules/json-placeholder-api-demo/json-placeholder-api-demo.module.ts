import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';

import { JsonPlaceholderApiDemoRoutingModule } from './json-placeholder-api-demo-routing.module';
import { JsonPlaceholderApiDemoComponent } from './json-placeholder-api-demo.component';
import { TodosComponent } from './views/todos/todos.component';
import { UsersComponent } from './views/users/users.component';
import { PostsComponent } from './views/posts/posts.component';
import { TodosResolver } from './resolvers/todos.resolver';
import { TodoByIdResolver } from './resolvers/todo-by-id.resolver';
import { TodoComponent } from './views/todo/todo.component';
import { jpaInitialState, jpaReducer } from './+state';


@NgModule({
  declarations: [
    JsonPlaceholderApiDemoComponent,
    TodosComponent,
    UsersComponent,
    PostsComponent,
    TodoComponent
  ],
  imports: [
    CommonModule,
    JsonPlaceholderApiDemoRoutingModule,
    StoreModule.forFeature(
      'jpa',
      jpaReducer,
      { initialState: jpaInitialState }
    )
  ],
  providers: [
    TodosResolver,
    TodoByIdResolver
  ]
})
export class JsonPlaceholderApiDemoModule {}
