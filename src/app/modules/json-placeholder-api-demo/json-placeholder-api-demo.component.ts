import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ns-json-placeholder-api-demo',
  templateUrl: './json-placeholder-api-demo.component.html',
  styleUrls: ['./json-placeholder-api-demo.component.scss']
})
export class JsonPlaceholderApiDemoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
