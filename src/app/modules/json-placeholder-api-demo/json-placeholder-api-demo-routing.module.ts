import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JsonPlaceholderApiDemoComponent } from './json-placeholder-api-demo.component';
import { TodosComponent } from './views/todos/todos.component';
import { PostsComponent } from './views/posts/posts.component';
import { UsersComponent } from './views/users/users.component';
import { TodosResolver } from './resolvers/todos.resolver';
import { TodoByIdResolver } from './resolvers/todo-by-id.resolver';
import { TodoComponent } from './views/todo/todo.component';

const routes: Routes = [
  {
    path: '', component: JsonPlaceholderApiDemoComponent, children: [
      { path: 'todos', component: TodosComponent, resolve: { todos: TodosResolver } },
      { path: 'todos/:todoId', component: TodoComponent, resolve: { todo: TodoByIdResolver } },
      { path: 'posts', component: PostsComponent },
      { path: 'users', component: UsersComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JsonPlaceholderApiDemoRoutingModule {}
