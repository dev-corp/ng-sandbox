import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ns-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public title: string = 'ng-sandbox';

  constructor(private store: Store<any>,
              private translate: TranslateService) {
    this.translate.defaultLang = 'en';
  }
}
