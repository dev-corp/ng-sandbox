import { Store } from '@ngrx/store';
import { MemoizedSelector } from '@ngrx/store/src/selector';
import { Observable } from 'rxjs';
import { publishReplay, refCount, takeUntil } from 'rxjs/operators';

export class StoreUtil {
  // use to share the same store value across multiple AsyncPipe uses
  // (by default, AsyncPipe creates new subscription for each pipe occurrence)
  // more can be found here: https://itnext.io/the-magic-of-rxjs-sharing-operators-and-their-differences-3a03d699d255
  public static select<TStore, TResult>(store: Store<TStore>,
                                        selector: MemoizedSelector<TStore, TResult>,
                                        unsubscribe?: Observable<any>): Observable<TResult> {
    const state = store.select(selector).pipe(
      publishReplay(1),
      refCount()
    );
    if (unsubscribe) {
      state.pipe(takeUntil(unsubscribe));
    }
    return state;
  }
}
