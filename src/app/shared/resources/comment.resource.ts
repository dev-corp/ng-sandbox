import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'environments/environment';
import { Comment } from 'app/shared/models';

@Injectable({
  providedIn: 'root'
})
export class CommentResource {

  private readonly path: string = `${environment.apiUrl}/comments`;

  constructor(private http: HttpClient) {
  }

  public getComments(): Observable<Comment[]> {
    return this.http.get<Comment[]>(this.path);
  }

  public getCommentById(id: number): Observable<Comment> {
    return this.http.get<Comment>(`${this.path}/${id}`);
  }
}
