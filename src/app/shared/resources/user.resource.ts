import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'environments/environment';
import { User } from 'app/shared/models';

@Injectable({
  providedIn: 'root'
})
export class UserResource {

  private readonly path: string = `${environment.apiUrl}/users`;

  constructor(private http: HttpClient) {
  }

  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.path);
  }

  public getUserById(id: number): Observable<User> {
    return this.http.get<User>(`${this.path}/${id}`);
  }
}
