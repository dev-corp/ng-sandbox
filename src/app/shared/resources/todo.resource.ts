import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'environments/environment';
import { Todo } from 'app/shared/models';

@Injectable({
  providedIn: 'root'
})
export class TodoResource {

  private readonly path: string = `${environment.apiUrl}/todos`;

  constructor(private http: HttpClient) {
  }

  public getTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.path);
  }

  public getTodoById(id: number): Observable<Todo> {
    return this.http.get<Todo>(`${this.path}/${id}`);
  }
}
