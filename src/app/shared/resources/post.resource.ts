import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'environments/environment';
import { Post } from 'app/shared/models';

@Injectable({
  providedIn: 'root'
})
export class PostResource {

  private readonly path: string = `${environment.apiUrl}/posts`;

  constructor(private http: HttpClient) {
  }

  public getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.path);
  }

  public getPostById(id: number): Observable<Post> {
    return this.http.get<Post>(`${this.path}/${id}`);
  }
}
