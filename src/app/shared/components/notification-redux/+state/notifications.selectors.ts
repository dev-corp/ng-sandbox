import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';

import { Notification } from '../notification.interface';
import { NotificationsState } from './notifications.interface';

const selectNotificationsFeature: MemoizedSelector<object, NotificationsState> = createFeatureSelector<NotificationsState>('notifications');

export class FromNotificationsState {

  public static selectQueue: MemoizedSelector<NotificationsState, Notification[]> = createSelector(
    selectNotificationsFeature,
    (state) => state.queue
  );

  public static selectCurrentNotification: MemoizedSelector<NotificationsState, Notification> = createSelector(
    selectNotificationsFeature,
    (state) => state.currentNotification
  );
}
