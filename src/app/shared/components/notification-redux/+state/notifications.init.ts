import { NotificationsState } from './notifications.interface';
import { Notification } from '../notification.interface';

export const notificationsInitialState: NotificationsState = {
  queue: [],
  currentNotification: null
};

export const defaultNotificationConfig: Notification = {
  message: null,
  closable: true,
  retryAction: null,
  type: 'info',
  duration: 5000
};
