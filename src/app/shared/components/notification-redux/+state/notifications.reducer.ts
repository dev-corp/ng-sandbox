import { NotificationsState } from './notifications.interface';
import { NotificationsAction, NotificationsActionTypes } from './notifications.actions';

export function notificationReducer(state: NotificationsState, action: NotificationsAction): NotificationsState {
  switch (action.type) {
    case NotificationsActionTypes.ADD_NOTIFICATION: {
      return { ...state };
    }
    case NotificationsActionTypes.NOTIFICATION_QUEUED: {
      // return { ...state, ...{ queue: [...state.queue, ...[action.payload]] } };
      return { ...state, ...{ queue: state.queue.concat(action.payload) } };
    }
    case NotificationsActionTypes.NEXT_NOTIFICATION: {
      return { ...state, ...{ currentNotification: action.payload } };
    }
    case NotificationsActionTypes.COMPLETE_NOTIFICATION: {
      return { ...state, ...{ currentNotification: null } };
    }
    default: {
      return state;
    }
  }
}
