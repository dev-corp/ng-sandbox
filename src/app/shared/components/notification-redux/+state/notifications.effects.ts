import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { filter, map, mergeMap, withLatestFrom } from 'rxjs/operators';

import { Notification } from '../notification.interface';

import { AddNotification, NotificationsActionTypes } from './notifications.actions';
import { FromNotificationsState } from './notifications.selectors';

@Injectable()
export class NotificationsEffects {

  @Effect()
  onAddNotification$: Observable<Action> = this.actions$
    .pipe(
      ofType(NotificationsActionTypes.ADD_NOTIFICATION),
      withLatestFrom(this.store.select(
        FromNotificationsState.selectCurrentNotification
      )),
      mergeMap(([action, currentNotification]: [AddNotification, Notification]) => {
        if (currentNotification) {
          return of({ type: NotificationsActionTypes.NOTIFICATION_QUEUED, payload: action.payload });
        } else {
          return of({ type: NotificationsActionTypes.NEXT_NOTIFICATION, payload: action.payload });
        }
      })
    );

  @Effect()
  onCompleteNotification$: Observable<Action> = this.actions$
    .pipe(
      ofType(NotificationsActionTypes.COMPLETE_NOTIFICATION),
      withLatestFrom(this.store.select(
        FromNotificationsState.selectQueue
      )),
      map(([action, queue]: [AddNotification, Notification[]]) => queue),
      filter(queue => queue.length > 0),
      mergeMap(queue => {
        return of({ type: NotificationsActionTypes.NEXT_NOTIFICATION, payload: queue.shift() });
      })
    );

  // NOTE: LEFT FOR EXAMPLE OF RETURNING NO ACTION
  // @Effect()
  // onCompleteNotification$: Observable<Action> = this.actions$
  //   .pipe(
  //     ofType(NotificationsActionTypes.COMPLETE_NOTIFICATION),
  //     withLatestFrom(this.store.select(
  //       FromNotificationsState.selectQueue
  //     )),
  //     mergeMap(([action, queue]: [AddNotification, Notification[]]) => {
  //       if (queue.length > 0) {
  //         return of({ type: NotificationsActionTypes.NEXT_NOTIFICATION, payload: queue.shift() });
  //       } else {
  //         return empty();
  //       }
  //     })
  //   );

  constructor(private actions$: Actions,
              private store: Store<any>) {
  }
}
