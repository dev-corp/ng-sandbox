import { Action } from '@ngrx/store';

import { Notification } from '../notification.interface';

export enum NotificationsActionTypes {
  ADD_NOTIFICATION = '[Notifications] ADD_NOTIFICATION',
  NOTIFICATION_QUEUED = '[Notifications] NOTIFICATION_QUEUED',
  NEXT_NOTIFICATION = '[Notifications] NEXT_NOTIFICATION',
  COMPLETE_NOTIFICATION = '[Notifications] COMPLETE_NOTIFICATION'
}

export class AddNotification implements Action {
  readonly type = NotificationsActionTypes.ADD_NOTIFICATION; // tslint:disable-line:typedef

  constructor(public payload: Notification) {}
}

export class NotificationQueued implements Action {
  readonly type = NotificationsActionTypes.NOTIFICATION_QUEUED; // tslint:disable-line:typedef

  constructor(public payload: Notification) {}
}

export class NextNotification implements Action {
  readonly type = NotificationsActionTypes.NEXT_NOTIFICATION; // tslint:disable-line:typedef

  constructor(public payload: Notification) {}
}

export class CompleteNotification implements Action {
  readonly type = NotificationsActionTypes.COMPLETE_NOTIFICATION; // tslint:disable-line:typedef
}

export type NotificationsAction = AddNotification | NotificationQueued | NextNotification | CompleteNotification;
