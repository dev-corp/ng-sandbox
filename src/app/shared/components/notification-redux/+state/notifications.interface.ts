import { Notification } from '../notification.interface';

export interface NotificationsState {
  queue: Notification[];
  currentNotification: Notification;
}
