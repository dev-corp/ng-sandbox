import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TranslateModule } from '@ngx-translate/core';
import { SnackbarModule } from 'material-components-angular';

import { NotificationComponent } from './notification.component';

import { notificationReducer, NotificationsEffects, notificationsInitialState } from './+state';

@NgModule({
  declarations: [NotificationComponent],
  imports: [
    CommonModule,
    SnackbarModule,
    TranslateModule,
    StoreModule.forFeature(
      'notifications',
      notificationReducer,
      { initialState: notificationsInitialState }
    ),
    EffectsModule.forFeature([NotificationsEffects])
  ],
  exports: [NotificationComponent]
})
export class NotificationModule {}
