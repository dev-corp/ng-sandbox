import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import { defaultNotificationConfig, FromNotificationsState, NotificationsActionTypes } from './+state';
import { NotificationConfig, Notification } from './notification.interface';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  constructor(private store: Store<any>) { }

  public addNotification(message: string, config?: NotificationConfig): void {
    const notification: Notification = {
      ...defaultNotificationConfig,
      ...config,
      ...{ message }
    };
    this.store.dispatch({ type: NotificationsActionTypes.ADD_NOTIFICATION, payload: notification });
  }

  public listenToNotifications(): Observable<Notification> {
    return this.store.select(FromNotificationsState.selectCurrentNotification)
      .pipe(filter(n => n != null));
  }

  public completeNotification(reason: string): void {
    this.store.dispatch({ type: NotificationsActionTypes.COMPLETE_NOTIFICATION, reason });
  }
}
