export interface NotificationConfig {
  retryAction?: () => void;
  closable?: boolean;
  type?: 'info' | 'warn' | 'error';
  duration?: number;
}

export interface Notification extends NotificationConfig {
  message: string;
}
