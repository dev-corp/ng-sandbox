import { NedCategory } from './ned-category';

export interface NEDData {
  id: number;
  category?: NedCategory;
  value?: string;
  totalDocs?: number;
  anchor?: boolean;
  selected?: boolean;
}
