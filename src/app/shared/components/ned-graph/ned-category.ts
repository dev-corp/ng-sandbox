export enum NedCategory {
  PERSON = 'PERSON',
  NORP = 'NORP',
  FAC = 'FAC',
  ORG = 'ORG',
  GPE = 'GPE',
  LOC = 'LOC',
  PRODUCT = 'PRODUCT',
  EVENT = 'EVENT',
  WORK_OF_ART = 'WORK_OF_ART',
  LAW = 'LAW',
  LANGUAGE = 'LANGUAGE',
  DATE = 'DATE',
  TIME = 'TIME',
  PERCENT = 'PERCENT',
  MONEY = 'MONEY',
  QUANTITY = 'QUANTITY',
  ORDINAL = 'ORDINAL',
  CARDINAL = 'CARDINAL'
}

export interface NedCategoryInfo {
  category: NedCategory;
  color: string;
  description: string;
}

export const nedCategoryInfo: NedCategoryInfo[] = [
  { color: '#FFD871', category: NedCategory.PERSON, description: 'People, including fictional.' },
  { color: '#6AA4E6', category: NedCategory.NORP, description: 'Nationalities or religious or political groups.' },
  { color: '#50C5C5', category: NedCategory.FAC, description: 'Buildings, airports, highways, bridges, etc.' },
  { color: '#579FFF', category: NedCategory.ORG, description: 'Companies, agencies, institutions, etc.' },
  { color: '#BFE572', category: NedCategory.GPE, description: 'Countries, cities, states.' },
  { color: '#6CEBFE', category: NedCategory.LOC, description: 'Non-GPE locations, mountain ranges, bodies of water.' },
  { color: '#FF7B23', category: NedCategory.PRODUCT, description: 'Objects, vehicles, foods, etc. (Not services.)' },
  { color: '#FFC56F', category: NedCategory.EVENT, description: 'Named hurricanes, battles, wars, sports events, etc.' },
  { color: '#FF78E9', category: NedCategory.WORK_OF_ART, description: 'Titles of books, songs, etc.' },
  { color: '#A067F6', category: NedCategory.LAW, description: 'Named documents made into laws.' },
  { color: '#F6AA67', category: NedCategory.LANGUAGE, description: 'Any named language.' },
  { color: '#B8DEFF', category: NedCategory.DATE, description: 'Absolute or relative dates or periods.' },
  // { color: '#56B2FF', category: NedCategory.TIME, description: 'Times smaller than a day.' },
  // { color: '#7385C1', category: NedCategory.PERCENT, description: 'Percentage, including "%".' },
  // { color: '#66DB00', category: NedCategory.MONEY, description: 'Monetary values, including unit.' },
  // { color: '#FFFA6F', category: NedCategory.QUANTITY, description: 'Measurements, as of weight or distance.' },
  // { color: '#CEE500', category: NedCategory.ORDINAL, description: '"first", "second", etc.' },
  // { color: '#06C225', category: NedCategory.CARDINAL, description: 'Numerals that do not fall under another type.' }
];
