// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
//
// import { NedGraphComponent } from './ned-graph.component';
// import { EdFacetExplorerService } from 'app/shared/ed-facet-explorer/ed-facet-explorer.service';
// import { Subject } from 'rxjs/Subject';
//
// describe('NedGraphComponent', () => {
//   let component: NedGraphComponent;
//   let fixture: ComponentFixture<NedGraphComponent>;
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [NedGraphComponent],
//       providers: [
//         {
//           provide: EdFacetExplorerService, useValue: {
//             modifiedSearchHasId: () => false,
//             checkChanges: () => { },
//             getChangeCheck: () => new Subject<any>()
//           }
//         }
//       ]
//     })
//       .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(NedGraphComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
