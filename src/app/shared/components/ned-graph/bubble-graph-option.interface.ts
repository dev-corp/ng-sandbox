export interface BubbleGraphOptions {
  width: number;
  height: number;
  linkForce: number;
  centerForce: number;
  velocityDecay: number;
  nodeDistance: number;
  minRadius: number;
  maxRadius: number;
  minBubblesForAnchor: number;
  lineHeight: number;
  labelPadding: number;
}
