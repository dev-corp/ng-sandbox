import { SimulationNodeDatum } from 'd3-force';

export interface Bubble extends SimulationNodeDatum {
  r?: number; // radius
}
