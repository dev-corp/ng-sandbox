import { AfterContentInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Selection, Simulation, SimulationLinkDatum } from 'd3';
import * as d3 from 'd3';

import { NEDGraphData } from './ned-graph-data.interface';
import { BubbleGraphOptions } from './bubble-graph-option.interface';
import { NedCategory, nedCategoryInfo } from './ned-category';
import { NEDData } from './ned-data.interface';

@Component({
  selector: 'ns-ned-graph',
  templateUrl: './ned-graph.component.html',
  styleUrls: ['./ned-graph.component.scss']
})
export class NedGraphComponent implements OnInit, AfterContentInit {

  @Input()
  set data(value: NEDGraphData[]) {
    this.localData = value;

    if (this.componentInitialized) {
      this.parseData();
      this.drawNplGraph();
    }
  }

  public selected: any[] = [];
  public preview: any = null;

  public links: SimulationLinkDatum<NEDGraphData>[];

  public categories: NedCategory[];

  public alpha: number = 0.5;
  public noLinks: boolean;
  public staticChart: boolean;
  public staticChartTicks: number = 400;
  public offScreen: number = 15;

  @ViewChild('graphContainer', { static: true })
  public graphContainer: ElementRef;

  public localData: NEDGraphData[];

  private options: BubbleGraphOptions;

  private graph: Selection<any, any, any, any>;
  private svg: Selection<any, any, any, any>;
  private node: Selection<any, any, any, any>;
  private bubble: Selection<any, any, any, any>;
  private bubbleText: Selection<any, any, any, any>;
  private tooltip: Selection<any, any, any, any>;
  private simulation: Simulation<NEDGraphData, undefined>;

  private componentInitialized: boolean;

  constructor(private elementRef: ElementRef) { }

  ngOnInit(): void {
    // PIOTR'S COMMENTS: Use this object to play around with different values and see how the graph will behave
    // The most important values are linkForce, centerForce, velocityDecay, nodeDistance
    // =======================================
    // Graph configuration
    // =======================================
    this.options = {
      width: this.graphContainer.nativeElement.offsetWidth - 17, // For some reason on first init the with is by 17 bigger
      height: 250,
      linkForce: 1,
      centerForce: 2,
      velocityDecay: 0.7, // PIOTR'S COMMENTS: how the bubbles are slowing down to finally stops
      nodeDistance: 2,
      minRadius: 25,
      maxRadius: 50,
      minBubblesForAnchor: 3,
      lineHeight: 15,
      labelPadding: 5,
    };
    this.componentInitialized = true;
    this.parseData();
  }

  ngAfterContentInit(): void {
    this.drawNplGraph();
  }

  public redraw(): void {
    this.parseData();
    this.drawNplGraph();
  }

  private parseData(): void {
    this.localData = this.produceNodes(this.localData, this.options.height, this.options.width);
    this.links = this.noLinks ? [] : this.produceLinks(this.localData, this.categories);
  }

  private produceNodes(data: NEDData[], height: number, width: number): NEDGraphData[] {
    const tempData = data.map(x => x.category);
    this.categories = tempData.filter((v, i) => tempData.indexOf(v) === i);

    const totalDocsArray = data.map(x => x.totalDocs);
    const maxTotalDocs = Math.max(...totalDocsArray);
    const minTotalDocs = Math.min(...totalDocsArray);

    // Calculate radius size based on total docs
    return data.map(x => {
      const catId = this.categories.indexOf(x.category);
      const nedGraphObject: NEDGraphData = x;
      nedGraphObject.r = this.countBubbleRadius(x, maxTotalDocs, minTotalDocs);
      nedGraphObject.x = width / (this.categories.length + 1) * catId + (width / (this.categories.length + 1));
      nedGraphObject.y = height / 2 * (catId % 2) + (height / 4);
      return nedGraphObject;
    });
  }

  private countBubbleRadius(nedItem: NEDData, maxTotalDocs: number, minTotalDocs: number): number {
    // Get totalDocs percentage in relation to max min docs of data set
    const baseDocsCount = maxTotalDocs - minTotalDocs; // this is the 100% of docs
    const itemDocsPercentage = baseDocsCount ? nedItem.totalDocs * 100 / baseDocsCount : 100;

    const radiusBase = this.options.maxRadius - this.options.minRadius;
    const radius = radiusBase * itemDocsPercentage / 100;

    return radius + this.options.minRadius;
  }

  // PIOTR'S COMMENTS: This creates links between nodes
  // It takes the first node and makes it an anchor to which all other nodes will be linked
  // This makes the effect that the group nodes sticks together
  private produceLinks(data: NEDGraphData[], categories: NedCategory[]): SimulationLinkDatum<NEDGraphData>[] {
    const links = [];
    categories.forEach(category => {
      const categoryNodes = data.filter(x => x.category.toString() === category);
      categoryNodes[0].anchor = true;
      if (categoryNodes.length > this.options.minBubblesForAnchor) {
        // Lock anchors
        // categoryNodes[0].fx = categoryNodes[0].x; // locks horizontal position
        categoryNodes[0].fy = categoryNodes[0].y; // locks vertical position
      }
      // Create links between anchor (first element from category) to every other element within category
      for (let i = 1; i < categoryNodes.length; i++) {
        categoryNodes[i].x += Math.floor(Math.random() * 5 * (Math.random() > 0.5 ? 1 : -1));
        categoryNodes[i].y += Math.floor(Math.random() * 5 * (Math.random() > 0.5 ? 1 : -1));
        links.push({ source: categoryNodes[0].id, target: categoryNodes[i].id });
      }
    });

    return links;
  }

  private drawNplGraph(): void {
    // Category color settings
    const color = d3.scaleOrdinal()
      .domain(nedCategoryInfo.map(x => x.category))
      .range(nedCategoryInfo.map(x => x.color));

    this.graph = d3.select('.ned-graph');

    this.svg = this.graph.select('.ned-graph-area')
      .attr('height', this.options.height)
      .attr('width', this.options.width);

    // Reset existing graph
    this.svg.selectAll('g').remove();

    this.node = this.svg.selectAll('g')
      .data(this.localData)
      .enter()
      .append('g')
      .attr('class', 'node');

    this.bubble = this.node.append('circle')
      .attr('class', 'bubble')
      .attr('cx', d => d.x)
      .attr('cy', d => d.y)
      .attr('r', d => d.r)
      .attr('stroke', d => {
        return nedCategoryInfo
          .filter(x => x.category === d.category)
          .map(x => x.color)[0];
      })
      .attr('fill', d => '' + color(d.category));

    this.bubbleText = this.node.append('text')
      .attr('class', 'bubble-text')
      .attr('x', (d, i, nodes) => d.x - (nodes[i].getComputedTextLength() / 2))
      .attr('y', (d, i, nodes) => d.y)
      .text(d => d.value)
      .call((selection) => {
        // =======================================
        // Split label for words wrapping within bubble
        // =======================================
        selection.each((d, i, nodes) => {
          const self = nodes[i];
          const text = d3.select(self);

          const words = text.text().split(/\s+/).reverse();
          let line = [];
          let tspan = text.text(null).append('tspan');

          let word;
          // tslint:disable-next-line:no-conditional-assignment
          while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(' '));
            if (tspan.node().getComputedTextLength() > (d.r * 2 - this.options.labelPadding) && line.length > 1) {
              line.pop();
              tspan.text(line.join(' '));
              line = [word];
              tspan = text.append('tspan')
                .text(word);
            }
          }
        });
      });

    // =======================================
    // Tick method that recalculate position for bubbles.
    // (!) Do not move outside drawNplGraph method
    // =======================================
    const tick = () => {
      this.bubble.attr('cx', d => d.x = Math.max(d.r - this.offScreen, Math.min(this.options.width - d.r + this.offScreen, d.x)));
      this.bubble.attr('cy', d => d.y = Math.max(d.r - this.offScreen, Math.min(this.options.height - d.r + this.offScreen, d.y)));

      this.bubbleText.attr('inner', (d, i, textNodes) => {
        const selft = textNodes[i];
        const text = d3.select(selft);
        const tspans: Selection<any, any, any, any> = text.selectAll('tspan');

        const linesCount = tspans.nodes().length;

        tspans.each((f, j, tspanNodes: SVGTextContentElement[]) => {
          const tspan = d3.select(tspanNodes[j]);
          tspan.attr('y', d.y - this.options.lineHeight * (linesCount - 1) / 3 + this.options.lineHeight * j);
          tspan.attr('x', d.x - (tspanNodes[j].getComputedTextLength() / 2));
        });

        return null;
      });
    };

    this.simulation = d3.forceSimulation<NEDGraphData, undefined>()
      .nodes(this.localData)
      .velocityDecay(this.options.velocityDecay)
      // Force - pull bubbles in the center stronger if there are less bubbles
      .force('x', d3.forceX()
        .x(this.options.width / 2)
        .strength(this.options.centerForce / this.localData.length)
      )
      // Links
      .force('link', d3.forceLink<NEDGraphData, SimulationLinkDatum<NEDGraphData>>(this.links)
        .id(d => {
          return d.id.toString();
        })
        .strength(this.options.linkForce)
        .distance(this.options.nodeDistance)
      )
      // Collision
      .force('collision', d3.forceCollide()
        .radius((d: NEDGraphData) => d.r + this.options.nodeDistance)
      )
      .on('tick', tick);

    // PIOTR'S COMMENTS: Uncomment this line to hide animation effect
    if (this.staticChart) {
      this.simulation.tick(this.staticChartTicks);
    }

    this.tooltip = this.graph.select('.ned-tooltip');

    this.node
      .on('mouseover', (d) => {
        this.tooltip.style('visibility', 'visible');
        this.tooltip.select('.ned-category')
          .text(d.category);
        this.tooltip.select('.ned-value')
          .text(d.value);
        this.tooltip.select('.ned-total-docs')
          .text(d.totalDocs);
      })
      .on('mousemove', () => {
        const tooltipWidth = this.tooltip.node().clientWidth;
        const x = d3.event.layerX - tooltipWidth / 2;
        const y = d3.event.layerY + 30;
        this.tooltip.style('top', y + 'px').style('left', x + 'px');
      })
      .on('mouseout', () => {
        this.tooltip.style('visibility', 'hidden');
      })
      .on('click', (d, i, nodes) => {
        d3.event.stopPropagation();
        d.selected = !d.selected;
        d3.select(nodes[i]).classed('selected', d.selected);
        this.preview = d;
        if (d.selected) {
          this.selected.push(d.value);
        } else {
          this.selected.splice(this.selected.indexOf(d.value), 1);
        }
        // this.edFacetExplorerService.updateSelectedFacets('', null);
      });

    // =======================================
    // Reheat simulation
    // =======================================
    // PIOTR'S COMMENTS: very useful to debug how the bubbles will behave
    this.svg.on('click', (d, i, nodes) => {
      this.simulation
        .restart()
        .alpha(this.alpha);
    });
  }
}
