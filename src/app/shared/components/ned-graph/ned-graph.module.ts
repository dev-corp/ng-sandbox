import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { NedGraphComponent } from './ned-graph.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    FormsModule,
    CommonModule
  ],
  declarations: [NedGraphComponent],
  exports: [NedGraphComponent],
  providers: [],
})
export class NedGraphModule {}
