import { NEDData } from './ned-data.interface';
import { Bubble } from './bubble.interface';


export interface NEDGraphData extends Bubble, NEDData {}
