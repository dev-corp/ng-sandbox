import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { publishReplay, refCount } from 'rxjs/operators';

import { Notification, QueuedNotification } from './notification.interface';

const initialConfig: Notification = {
  message: null,
  closable: true,
  retryAction: null,
  type: 'info',
  duration: 5000
};

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private notificationStream$: Subject<QueuedNotification> = new Subject();
  private notificationQueue: QueuedNotification[] = [];

  constructor() { }

  public addNotification(message: string, config?: {
    closable?: boolean,
    retryAction?: () => void,
    type?: 'info' | 'warn' | 'error',
    duration?: number;
  }): void {
    const notification: QueuedNotification = {
      id: Date.now(),
      ...initialConfig,
      ...config,
      ...{ message }
    };
    this.addToQueue(notification);
  }

  public listenToNotifications(): Observable<QueuedNotification> {
    return this.notificationStream$
      .pipe(
        publishReplay(1),
        refCount()
      );
  }

  public completeNotification(notificationId: number): void {
    const idx = this.notificationQueue.findIndex(n => n.id === notificationId);
    if (idx > -1) {
      this.notificationQueue.splice(idx, 1);
      if (this.notificationQueue.length > 0) {
        this.notificationStream$.next(this.notificationQueue[0]);
      }
    }
  }

  private addToQueue(notification: QueuedNotification): void {
    this.notificationQueue.push(notification);
    if (this.notificationQueue.length === 1) {
      this.notificationStream$.next(notification);
    }
  }
}
