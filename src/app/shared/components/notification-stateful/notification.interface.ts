export interface Notification {
  message: string;
  retryAction?: () => void;
  closable?: boolean;
  type?: 'info' | 'warn' | 'error';
  duration?: number;
}

export interface QueuedNotification extends Notification {
  id: number;
}
