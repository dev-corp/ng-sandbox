import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { SnackbarDirective } from 'material-components-angular';

import { NotificationService } from './notification.service';
import { QueuedNotification } from './notification.interface';
import { NgUnsubscribe } from 'app/shared/utilities';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ns-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationComponent extends NgUnsubscribe implements OnInit {

  public notification: QueuedNotification;

  @ViewChild(SnackbarDirective, { static: true })
  public snackbar: SnackbarDirective;

  constructor(private cdRef: ChangeDetectorRef,
              private notificationService: NotificationService) {
    super();
  }

  ngOnInit(): void {
    this.notificationService.listenToNotifications()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(notification => {
        this.notification = notification;
        this.cdRef.detectChanges();
        if (this.snackbar) { this.snackbar.open(); }
      });
  }

  public close(reason?: string): void {
    if (reason) {
      this.snackbar.close(reason);
    } else {
      this.snackbar.close();
    }
  }

  public retry(notification: QueuedNotification): void {
    notification.retryAction();
    this.close('retrying');
  }

  public onClosed(notification: QueuedNotification, reason: string): void {
    this.complete(notification);
  }

  private complete(notification: QueuedNotification): void {
    this.notificationService.completeNotification(notification.id);
  }
}
