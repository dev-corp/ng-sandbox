import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SnackbarModule } from 'material-components-angular';

import { NotificationComponent } from './notification.component';

@NgModule({
  declarations: [NotificationComponent],
  imports: [
    CommonModule,
    SnackbarModule,
    TranslateModule
  ],
  exports: [NotificationComponent]
})
export class NotificationModule {}
