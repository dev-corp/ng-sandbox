this folder should contains all elements that are shared between modules/components
keep in mind that it should be well organized and files should be split depending on purpose

single files could be store directly in main folder, e.g. people.service could be stored in shared/service/**, even if there is corresponding .spec.ts file
while group of files (2+), like components, are better to stored in dedicated folder, e.g. shared/components/my-component/**

it is also good to use barrels -> index.ts files that exports elements from files within it's directory
this makes imports looks cleaner and easier to read
* not sure regards drawbacks of barrels. TODO: barrels drawbacks
