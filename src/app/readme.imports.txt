keep imports organized
use barrels from simplified imports and avoiding long duplicate words:
  no:       import { MyComponent } from 'app/shared/components/my-component/my-component.component.ts'
  yes:      import { MyComponent } from 'app/shared/components/my-component' // /index.ts
  better:   import { MyComponent } from 'app/shared/components' // /index.ts

**my habit**
separate node_module imports from shared imports from local elements imports
  *> see app.module.ts
