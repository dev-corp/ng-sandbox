import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { ActionReducer, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { localStorageSync } from 'ngrx-store-localstorage';
import { LoadingIndicatorModule } from 'ns-lib';

// *** separate node_modules from global app imports

import { environment } from 'environments/environment';
import { TranslationModule } from 'app/core/modules/translation.module';
import { AppEffects, appInitialState, appReducer } from 'app/core/+state';

// *** separate global app imports from local imports

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './core/components/navigation/navigation.component';

/*
export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({
    keys: [
      { app: [] },
    ],
    rehydrate: true, // load state from storage
    storage: sessionStorage,
    storageKeySerializer: (key) => `ns_${environment.production ? '' : 'dev'}_state_${key}`,
  })(reducer);
}
*/

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    // State
    StoreModule.forRoot(
      { app: appReducer },
      {
        metaReducers: [/*localStorageSyncReducer*/],
        initialState: { app: appInitialState }
      }
    ),
    EffectsModule.forRoot([AppEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    // StoreRouterConnectingModule.forRoot(),
    AppRoutingModule,
    TranslationModule,
    LoadingIndicatorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
