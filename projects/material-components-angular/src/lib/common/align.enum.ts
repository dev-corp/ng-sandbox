export enum Align {
  Left = 'left',
  Right = 'right',
  Top = 'top',
  Middle = 'middle',
  Bottom = 'bottom'
}
