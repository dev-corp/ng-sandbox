import { randomString } from 'projects/material-components-angular/src/lib/common/random-string.function';

describe('randomString', () => {
  it('should generate random string', () => {
    // Arrange
    // Act
    const result = randomString();
    // Assert
    expect(result).not.toBeNull();
    expect(result.length).toBeGreaterThan(0);
  });

  it('should always generate new random string', () => {
    // Arrange
    // Act
    const result1 = randomString();
    const result2 = randomString();
    // Assert
    expect(result1).not.toEqual(result2);
  });
});
