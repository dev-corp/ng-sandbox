import { AfterViewInit, ElementRef } from '@angular/core';

export abstract class MdcClassDirectiveBase<THTMLType extends HTMLElement> implements AfterViewInit {

  protected get element(): THTMLType {
    return this.elementRef.nativeElement;
  }

  protected constructor(protected elementRef: ElementRef) {
  }

  ngAfterViewInit(): void {
    this.validate();
    this.updateClasses();
  }

  protected abstract updateClasses(): void ;

  protected abstract validate(): void;

}
