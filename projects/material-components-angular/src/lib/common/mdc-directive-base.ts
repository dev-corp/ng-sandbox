import { AfterViewInit, ElementRef, OnDestroy } from '@angular/core';

import { MdcClassDirectiveBase } from './mdc-class-directive-base';

export abstract class MdcDirectiveBase<THTMLType extends HTMLElement>
  extends MdcClassDirectiveBase<THTMLType>
  implements AfterViewInit, OnDestroy {

  protected constructor(protected elementRef: ElementRef) {
    super(elementRef);
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    this.setup();
  }

  ngOnDestroy(): void {
    for (const item of this.itemsToDestroy()) {
      item.destroy();
    }
  }

  protected abstract setup(): void ;

  protected abstract itemsToDestroy(): { destroy(): void } [];

}
