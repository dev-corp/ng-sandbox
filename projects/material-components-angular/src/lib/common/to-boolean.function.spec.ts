import { toBoolean } from './to-boolean.function';

describe('toBoolean', () => {

  it('should convert \'\' to true', () => {
    // Arrange
    const inputValue = '';
    // Act
    const result = toBoolean(inputValue);
    // Assert
    expect(result).toBeTruthy();
  });

  it('should convert true to true', () => {
    // Arrange
    const inputValue = true;
    // Act
    const result = toBoolean(inputValue);
    // Assert
    expect(result).toBeTruthy();
  });

  it('should convert false to false', () => {
    // Arrange
    const inputValue = false;
    // Act
    const result = toBoolean(inputValue);
    // Assert
    expect(result).toBeFalsy();
  });

  it('should convert \'false\' to false', () => {
    // Arrange
    const inputValue = 'false';
    // Act
    const result = toBoolean(inputValue);
    // Assert
    expect(result).toBeFalsy();
  });

  it('should convert null to false', () => {
    // Arrange
    const inputValue = null;
    // Act
    const result = toBoolean(inputValue);
    // Assert
    expect(result).toBeFalsy();
  });
});
