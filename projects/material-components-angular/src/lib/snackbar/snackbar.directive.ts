import { Directive, ElementRef, EventEmitter, Input, Output, Renderer2 } from '@angular/core';
import { MDCSnackbar } from '@material/snackbar/component';

import { MdcDirectiveBase } from '../common';

@Directive({
  selector: '[mdcSnackbar]'
})
export class SnackbarDirective extends MdcDirectiveBase<HTMLElement> {

  @Input()
  set timeout(value: number) {
    if (value < 4000) {
      this._timeout = 4000;
    } else if (value > 10000) {
      this._timeout = 10000;
    } else if (!value) {
      this._timeout = 5000;
    } else {
      this._timeout = value;
    }

    if (this.mdcSnackbar) { this.mdcSnackbar.timeoutMs = this._timeout; }
  }

  get timeout(): number { return this._timeout; }

  @Input()
  set closeOnEscape(value: boolean) {
    this._closeOnEscape = value;
    if (this.mdcSnackbar) { this.mdcSnackbar.closeOnEscape = this._closeOnEscape; }
  }

  get closeOnEscape(): boolean { return this._closeOnEscape; }

  @Output()
  public opening: EventEmitter<void> = new EventEmitter();

  @Output()
  public opened: EventEmitter<void> = new EventEmitter();

  @Output()
  public closing: EventEmitter<string> = new EventEmitter();

  @Output()
  public closed: EventEmitter<string> = new EventEmitter();

  private _timeout: number = 5000;
  private _closeOnEscape: boolean = true;

  public get snackbar(): MDCSnackbar {
    return this.mdcSnackbar;
  }

  private mdcSnackbar: MDCSnackbar;

  constructor(protected elementRef: ElementRef<HTMLDivElement>,
              private renderer: Renderer2) {
    super(elementRef);
  }

  public open(): void {
    this.mdcSnackbar.open();
  }

  public close(reason?: string): void {
    this.mdcSnackbar.close(reason);
  }

  protected itemsToDestroy(): { destroy(): void }[] {
    return [this.mdcSnackbar];
  }

  protected setup(): void {
    this.mdcSnackbar = new MDCSnackbar(this.element);
    // Configuration
    this.mdcSnackbar.timeoutMs = this.timeout;
    this.mdcSnackbar.closeOnEscape = this.closeOnEscape;
    this.registerEventListeners();
  }

  protected updateClasses(): void {
    if (!this.element.classList.contains('mdc-snackbar')) {
      this.renderer.addClass(this.element, 'mdc-snackbar');
    }
  }

  protected validate(): void {
  }

  private registerEventListeners(): void {
    this.mdcSnackbar.listen('MDCSnackbar:opening', () => {
      this.opening.emit();
    });

    this.mdcSnackbar.listen('MDCSnackbar:opened', () => {
      this.opened.emit();
    });

    this.mdcSnackbar.listen('MDCSnackbar:closing', (event: CustomEvent) => {
      this.closing.emit(event.detail.reason);
    });

    this.mdcSnackbar.listen('MDCSnackbar:closed', (event: CustomEvent) => {
      this.closed.emit(event.detail.reason);
    });
  }
}
