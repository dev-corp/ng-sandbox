import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SnackbarDirective } from './snackbar.directive';

@NgModule({
  declarations: [SnackbarDirective],
  imports: [
    CommonModule
  ],
  exports: [SnackbarDirective],
})
export class SnackbarModule {}
