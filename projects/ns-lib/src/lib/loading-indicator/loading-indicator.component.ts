import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'nsl-loading-indicator',
  templateUrl: './loading-indicator.component.html',
  styleUrls: ['./loading-indicator.component.scss']
})
export class LoadingIndicatorComponent implements OnInit {

  @Input() public show: boolean = false;
  @Input() public embedded: boolean = false;
  @Input() public center: boolean = false;
  @Input() public absolute: boolean = false;
  @Input() public styleClass: string;
  @Input() public spinnerPath: string;

  constructor() {}

  ngOnInit(): void {
    if (!this.spinnerPath) {
      throw new Error('[spinnerPath] is required parameter in loading-indicator');
    }
  }
}
