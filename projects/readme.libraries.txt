..::libraries live reload::..
in order to make libraries love reload (not to have to build them each time) there are two things to do:
1.
  create index.ts fine under ns-lib/src/index.ts
  with content
      export * from public-api.ts
2.
  update tsconfig.json file
      "paths": {
        "ns-lib": [
          "dist/ns-lib" => "..projects/ns-lib/src"
        ],
        "ns-lib/*": [
          "dist/ns-lib/*" => "..projects/ns-lib/src/*"
        ]
      }

more here: https://mereommig.dk/en/blog/adding-livereload-to-the-angular-cli-libraries
